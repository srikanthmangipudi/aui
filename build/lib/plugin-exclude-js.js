'use strict';

var gulpFilter = require('gulp-filter');
var multimatch = require('multimatch');

var patterns = [
    // We must include all files first since we're negating with the rest of
    // the glob patterns.
    '**',

    // Exclude the batch AUI and AUI-ADG JavaScript files used for the dist
    // except for aui-css-deprecations.js because we want plugin consumers to
    // be able to use that during development.
    '!**/src/js/{aui,aui-datepicker,aui-experimental,aui-soy}.js',

    // Exclude compiled soy since we provide the compiled soy via the soy
    // transformer.
    '!**/compiled-soy/*.js'
];

module.exports = function () {
    return gulpFilter(patterns);
};
