var gat = require('gulp-auto-task');
var path = require('path');

module.exports = function () {
    var args = [].slice.call(arguments);
    var opts = gat.opts();
    return args.map(function (arg) {
        return path.join(opts.root, arg);
    }).concat(args);
};
