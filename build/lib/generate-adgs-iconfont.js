const path = require('path');
const glob = require('glob').sync;
const fs = require('fs');
const webfontsGenerator = require('webfonts-generator');

const glyphsMappings = require('./generate-adgs-iconfont/glyphs-mappings');

const rootPath = path.resolve(__dirname, '..', '..');
const iconsSrc = path.resolve(rootPath, 'src/icons/16px/');
const dest = path.resolve(rootPath, 'src/less/fonts');
const documentationIconsListPath = path.resolve(rootPath, 'docs/src/assets/icons-list.json');

const fontName = 'ADGS Icons';
const fontFileName = 'adgs-icons';
const fontId = 'adgs-icons';

const startCodepoint = 0xf1f3;

const templateOptions = {
    baseClassSelector: 'aui-iconfont',
    classPrefix: 'aui-iconfont-',
    baseSelector: '.aui-iconfont'
};

const htmlTemplate = path.resolve(__dirname, 'generate-adgs-iconfont/html.hbs');
const cssTemplate = path.resolve(__dirname, 'generate-adgs-iconfont/css.hbs');

const files = glob('**/*.svg', {
    cwd: iconsSrc
}).map(iconPath => path.resolve(iconsSrc, iconPath));

const codepoints = Object
    .keys(glyphsMappings)
    .map((unicode) => {
        const iconName = glyphsMappings[unicode];

        return {
            iconName,
            unicode: parseInt(unicode, 16)
        };
    }).reduce((result, { iconName, unicode }) => {
        if (result[iconName]) {
            result[iconName].push(unicode);
        } else {
            result[iconName] = [unicode];
        }

        return result;
    }, {});

const sanitizeName = name => name.toLowerCase().replace(/\s+/g, '-');

const rename = (iconPath) => {
    let name = path.basename(iconPath, path.extname(iconPath));

    return sanitizeName(name);
};

const options = {
    files, dest, fontName, fontId, templateOptions, codepoints, startCodepoint,
    rename,
    htmlTemplate,
    cssTemplate,
    css: true,
    html: true,
    normalize: true,
    fontHeight: 512,
    writeFiles: false
};

const saveFontResult = (format, content) => {
    const filePath = path.resolve(dest, `${fontFileName}.${format}`);

    fs.writeFileSync(filePath, content);

    return filePath;
};

const formats = ['svg', 'ttf', 'eot', 'woff'];

// Generate icons
webfontsGenerator(options, (error, result) => {
    if (error) {
        console.log('Fail!', error);
        process.exit(1);

        return;
    }

    formats.forEach(format => {
        const content = result[format];
        const filePath = saveFontResult(format, content);

        console.log(`Font ${format} saved to: "${filePath}"`)
    });

    // Save icon list for documentation purpose
    fs.writeFileSync(documentationIconsListPath, JSON.stringify(files.map(rename), null, '  '));

    console.log('Done!');
});
