'use strict';

var gat = require('gulp-auto-task');
var gulp = require('gulp');
var path = require('path');
var pkg = require('../../../package.json');
var zip = require('gulp-zip');

function compressDist () {
    var distPath = path.resolve('dist');
    var outDir = path.resolve('.tmp');
    return gulp.src(path.join(distPath, '**'))
        .pipe(zip(`aui-flat-pack-${pkg.version}.zip`))
        .pipe(gulp.dest(outDir));
}

module.exports = gulp.series(
    gat.load('dist'),
    compressDist
);
