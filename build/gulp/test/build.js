'use strict';

var galv = require('galvatron');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var gulpBabel = require('gulp-babel');
var gulpConcat = require('gulp-concat');
var gulpDebug = require('gulp-debug');
var gulpIf = require('gulp-if');
var isEs6 = require('../../lib/is-es6');
var lazyPipe = require('lazypipe');
var libTraceMap = require('../../lib/trace-map');
var libVersionReplace = require('../../lib/version-replace');
var opts = gat.opts();

function buildTests () {
    var babelify = lazyPipe()
        .pipe(galv.cache, 'babel', gulpBabel());

    return galv.trace('tests/unit.js', {
        map: libTraceMap(opts.root)
    }).createStream()
        .pipe(gulpDebug({
            title: 'compile'
        }))
        .pipe(libVersionReplace())
        .pipe(gulpIf(isEs6, babelify()))
        .pipe(galv.cache('globalize', galv.globalize()))
        .pipe(gulpConcat('unit.js'))
        .pipe(gulp.dest('.tmp'))
}

module.exports = gulp.series(
    gat.load('soy'),
    buildTests
);
