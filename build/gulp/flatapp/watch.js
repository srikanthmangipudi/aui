var galv = require('galvatron');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var gulpWebserver = require('gulp-webserver');

let opts = gat.opts();
let { host, port } = Object.assign({
    host: '0.0.0.0',
    port: 7000
}, opts);

var taskFlatapp = gat.load('flatapp/webpack');

module.exports = gulp.series(
    taskFlatapp,
    function flatappWatch (done) {
        gulp.watch([
            'src/**',
            'tests/flatapp/src/**',
            'tests/test-pages/**'
        ], taskFlatapp).on('change', galv.cache.expire);
        done();
    },
    function flatappServer () {
        return gulp.src('.tmp/flatapp/target/static')
            .pipe(gulpWebserver({
                host,
                port,
                livereload: false,
                open: 'pages',
            }));
    }
);
