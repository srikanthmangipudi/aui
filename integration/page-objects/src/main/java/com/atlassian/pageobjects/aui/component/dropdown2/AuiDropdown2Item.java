package com.atlassian.pageobjects.aui.component.dropdown2;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementActions;
import org.openqa.selenium.By;

import javax.inject.Inject;
/**
 * @deprecated Previously published API deprecated as of AUI 5.0. Do not use outside AUI. Will be refactored out eventually.
 */
@Deprecated
public class AuiDropdown2Item
{

    private final PageElement dropdownItem;
    private PageElement dropdownItemAnchor;

    @Inject
    private PageElementActions actions;

    @Inject
    PageBinder binder;

    public AuiDropdown2Item(PageElement dropdownItem)
    {
        this.dropdownItem = dropdownItem;
    }

    @Init
    public void init()
    {
        dropdownItemAnchor = dropdownItem.find(By.tagName("a"));
    }

    public void hoverOver()
    {
        actions.moveToElement(dropdownItemAnchor).perform();
    }

    public AuiDropdown2 getSubmenu()
    {
        return binder.bind(AuiDropdown2.class, dropdownItemAnchor);
    }

    public boolean isClipped()
    {
        return (Boolean) dropdownItemAnchor.javascript().execute("var el = arguments[0]; return el.scrollWidth > el.clientWidth;", dropdownItemAnchor);
    }

//    public boolean ariaChecked()
//    {
//        String ariaChecked = dropdownItemAnchor.getAttribute("aria-checked");
//        Preconditions.checkState(ariaChecked != null, "The aria-checked attribute must be defined on the dropdown2 checkbox item");
//        return "true".equals(ariaChecked);
//
//        // expected to use getAttribute or hasAttribute with a value but had trouble with the syntax - said getAttribute needed a boolean?!
////        return dropdownItemAnchor.javascript().execute("jQuery(this).attr('aria-checked')", Boolean.class, dropdownItemAnchor).now();
//    }

}
