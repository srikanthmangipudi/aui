package com.atlassian.aui.test.contacts.rest;

import java.util.HashMap;
import java.util.Map;

public class Result<T>
{

    final private Map<String, String> errors;
    private T result;

    Result(final T result, final Map<String, String> errors)
    {
        this.result = result;
        this.errors = errors;
    }

    public Map<String, String> getErrors()
    {
        return errors;
    }

    public boolean isValid()
    {
        return errors.isEmpty();
    }

    public Result<T> setResult(T result)
    {
        this.result = result;
        return this;
    }

    public T getResult()
    {
        return result;
    }

    static class Builder<T>
    {

        private Map<String, String> errors = new HashMap<String, String>();
        private T result;

        Builder<T> setResult(T t)
        {
            this.result = t;
            return this;
        }

        Builder<T> addError(String id, String message)
        {
            errors.put(id, message);
            return this;
        }

        public Result<T> build()
        {
            return new Result<T>(result, errors);
        }
    }
}
