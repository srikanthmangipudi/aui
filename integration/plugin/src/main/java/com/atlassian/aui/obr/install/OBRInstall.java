package com.atlassian.aui.obr.install;

/**
 * This class is here just so we can (OSGi) export a package that can then be imported by another plugin.
 * This is required for the purpose of OBR installation.
 * A new package is deliberately introduced to minimise the dependencies.
 */
public class OBRInstall {
}
