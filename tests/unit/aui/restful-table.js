'use strict';

import '../../../src/js/aui';
import $ from '../../../src/js/aui/jquery';
import RestfulTable from '../../../src/js/aui/restful-table';

// For Checking global existence.
import RestfulTableClassNames from '../../../src/js/aui/restful-table/class-names';
import RestfulTableCustomCreateView from '../../../src/js/aui/restful-table/custom-create-view';
import RestfulTableCustomEditView from '../../../src/js/aui/restful-table/custom-edit-view';
import RestfulTableCustomReadView from '../../../src/js/aui/restful-table/custom-read-view';
import RestfulTableDataKeys from '../../../src/js/aui/restful-table/data-keys';
import RestfulTableEditRow from '../../../src/js/aui/restful-table/edit-row';
import RestfulTableEntryModel from '../../../src/js/aui/restful-table/entry-model';
import RestfulTableEvents from '../../../src/js/aui/restful-table/events';
import RestfulTableRow from '../../../src/js/aui/restful-table/row';
import RestfulTableThrobber from '../../../src/js/aui/restful-table/throbber';

describe('aui/restful-table', function () {
    it('globals', function () {
        expect(AJS.RestfulTable).to.equal(RestfulTable);

        expect(AJS.RestfulTable.ClassNames).to.equal(RestfulTableClassNames);
        expect(AJS.RestfulTable.CustomCreateView).to.equal(RestfulTableCustomCreateView);
        expect(AJS.RestfulTable.CustomEditView).to.equal(RestfulTableCustomEditView);
        expect(AJS.RestfulTable.CustomReadView).to.equal(RestfulTableCustomReadView);
        expect(AJS.RestfulTable.DataKeys).to.equal(RestfulTableDataKeys);
        expect(AJS.RestfulTable.EditRow).to.equal(RestfulTableEditRow);
        expect(AJS.RestfulTable.EntryModel).to.equal(RestfulTableEntryModel);
        expect(AJS.RestfulTable.Events).to.equal(RestfulTableEvents);
        expect(AJS.RestfulTable.Row).to.equal(RestfulTableRow);
        expect(AJS.RestfulTable.Throbber).to.equal(RestfulTableThrobber);
    });

    describe('Initialization', function () {
        var server;
        var rt;
        var rtRows;
        var $table;

        beforeEach(function () {
            server = sinon.fakeServer.create();
            var users = [{'id': 1, 'name': 'adam'},{'id': 2, 'name': 'betty'},{'id': 3, 'name': 'chris'}];

            server.respondWith('GET', '/all', [200, {'Content-Type': 'application/json'}, JSON.stringify(users)]);

            rt = new RestfulTable({
                el: $('<table id="test-table" class="aui"></table>'),
                resources: {
                    all: '/all',
                    self: '/single'
                },
                columns: [
                    {
                        id: 'name',
                        header: 'Name'
                    }
                ]
            });

            rtRows = [];
            rt.bind(RestfulTable.Events.ROW_INITIALIZED, function (row) {
                rtRows.push(row);
            });

            $table = rt.getTable();
            $table.appendTo('#test-fixture');

            server.respond();
        });

        afterEach(function () {
            server.restore();
        });

        it('renders properly', function () {
            expect($table.length).to.equal(1);
            expect(rtRows.length).to.equal(3);

            expect($table.find('thead th:first').text()).to.equal('Name');
            expect($table.find('tbody.aui-restfultable-create tr').length).to.equal(1);
            expect($table.find('tbody:not(.aui-restfultable-create) tr').length).to.equal(3);
        });

        it('test - proper row data is used', function () {
            expect(rtRows[0].model.get('id')).to.equal(1);
            expect(rtRows[0].model.get('name')).to.equal('adam');

            expect($table.find('tbody:not(.aui-restfultable-create) tr:eq(0)').data('id')).to.equal(1);
            expect($table.find('tbody:not(.aui-restfultable-create) tr:eq(0)').data('name')).to.equal('adam');
            expect($table.find('tbody:not(.aui-restfultable-create) tr:eq(1) td:first').text()).to.equal('betty');
        });

        it('test - edit works', function () {
            var row = rtRows[0];
            var edited = rt.edit(row, 'name');

            edited.$('input[name=name]').val('edited');
            edited.submit(false);

            server.respondWith('PUT', '/single/1', function (xhr) {
                xhr.respond(200, {'Content-Type': 'application/json'}, xhr.requestBody);
            });
            server.respond();

            var editResponse = JSON.parse(server.requests[1].requestBody);
            expect(editResponse.name).to.equal('edited');
        });

        it('test - fieldFocusSelector is defined for create row', function () {
            var row = rtRows[0];
            var edited = rt.edit(row, 'name');
            edited.$('input[name=name]').focus();
            var createRow = rt.getCreateRow();
            createRow.focus('name');

            expect(createRow.$el.get(0).firstChild.firstChild).to.equal(document.activeElement);
        });
    });

    describe('Custom object serialization', function () {
        var server;
        var rt;
        var rtRows;
        var $table;

        beforeEach(function () {
            server = sinon.fakeServer.create();
            var users = [{'id': 1, 'name': 'adam'}];

            server.respondWith('GET', '/all', [200, {'Content-Type': 'application/json'}, JSON.stringify(users)]);
            rt = new RestfulTable({
                el: $('<table id="test-table" class="aui"></table>'),
                resources: {
                    all: '/all',
                    self: '/single'
                },
                columns: [
                    {
                        id: 'name',
                        header: 'Name'
                    }
                ],
                views: {
                    editRow: RestfulTable.EditRow.extend({
                        initialize: function () {
                            RestfulTable.EditRow.prototype.initialize.apply(this, Array.prototype.slice.call(arguments));
                        },
                        serializeObject: function () {
                            return {
                                name: this.$el.find(':input[name=name]').val() + ' serialized'
                            };
                        }
                    })
                }
            });

            rtRows = [];
            rt.bind(RestfulTable.Events.ROW_INITIALIZED, function (row) {
                rtRows.push(row);
            });

            $table = rt.getTable();
            $table.appendTo('#test-fixture');
            server.respond();
        });

        afterEach(function () {
            server.restore();
        });

        it('should serialize updated table input on edit', function () {
            var row = rtRows[0];
            var edited = rt.edit(row, 'name');
            edited.$('input[name=name]').val('edited');
            edited.submit(false);
            server.respondWith('PUT', '/single/1', function (xhr) {
                JSON.parse(xhr.requestBody);
                xhr.respond(200, {'Content-Type': 'application/json'}, xhr.requestBody);
            });
            server.respond();

            var editResponse = JSON.parse(server.requests[1].requestBody);
            expect(editResponse.name).to.equal('edited serialized');
        });
    });
});
