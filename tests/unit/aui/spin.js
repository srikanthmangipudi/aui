'use strict';

import $ from '../../../src/js/aui/jquery';
import helpers from '../../helpers/all';
import skate from '../../../src/js/aui/internal/skate';
import '../../../src/js/aui/button';
import '../../../src/js/aui/spin';

describe('aui/spin', () => {
    let $testFixture;
    const SPINNER_SELECTOR = 'aui-spinner';

    beforeEach(() => {
        $testFixture = $('#test-fixture');
    });

    describe('basic functionality - ', () => {
        let $buttonShow;
        let $buttonHide;
        let $spinner;

        beforeEach(() => {
            $buttonShow = $('<button id="show-spinner"></button>').appendTo($testFixture);
            $buttonHide = $('<button id="hide-spinner"></button>').appendTo($testFixture);
            $spinner = $('<span id="spinner"></span>').appendTo($testFixture);

            $buttonShow.on('click', $spinner.spin.bind($spinner));
            $buttonHide.on('click', $spinner.spinStop.bind($spinner));
        });

        it('should show spinner', () => {
            $buttonHide.click();
            $buttonShow.click();

            expect($(SPINNER_SELECTOR).length).to.equal(1);
        });

        it('should hide spinner', () => {
            $buttonShow.click();
            $buttonHide.click();

            expect($(SPINNER_SELECTOR).length).to.equal(0);
        });
    });

    describe('jQuery integration (deprecated) - ', () => {
        beforeEach(() => {
            $testFixture.append(`
                <span class="add-here"></span>
                <span class="not-here"></span>
                <span class="add-here"></span>
            `);
        });

        ['spin', 'spinStop'].forEach(method => {
            it(`${method}() should be tolerant of nonsensical selections`, () => {
                let $els = $([null, undefined, {}]);
                $els[method]();
                expect($(SPINNER_SELECTOR).length).to.equal(0);
            });

            it(`${method}() should support jQuery function chaining`, () => {
                let $els = $testFixture.find('span');
                let result = $els[method]();
                expect(result).to.be.an.instanceof($);
            });
        });

        describe('spin() ', () => {
            it('should add a spinner to each selected element', () => {
                let $els = $testFixture.find('.add-here');
                $els.spin();
                expect($(SPINNER_SELECTOR).length).to.equal(2);
            });

            it('should not add spinners when no elements are selected', () => {
                let $els = $testFixture.find('.nonexistent');
                $els.spin();
                expect($(SPINNER_SELECTOR).length).to.equal(0);
            });
        });

        describe('spinStop() ', () => {
            beforeEach(() => {
                $testFixture
                    .append('<span class="already-spinning"></span>')
                    .prepend('<span class="already-spinning"></span>');
                $testFixture.find('.already-spinning').spin();
                expect($(SPINNER_SELECTOR).length).to.equal(2);
            });

            it('should remove a spinner from each selected element', () => {
                let $els = $testFixture.find('.already-spinning');
                $els.spinStop();
                expect($(SPINNER_SELECTOR).length).to.equal(0);
            });
        });

        // This is effectively the case whenever a spinner was added by a means other than calling .spin()
        describe('$.data() returned null', () => {
            let dataFn;
            let $spinner;

            beforeEach(() => {
                $spinner = $('<span id="spinner"></span>').appendTo($testFixture);
                $spinner.spin();
                dataFn = $.fn.data;
                $.fn.data = () => null;
            });

            it('should not hide previous spinner', () => {
                $spinner.spinStop();
                expect($(SPINNER_SELECTOR).length).to.equal(1);
            });

            afterEach(() => {
                $.fn.data = dataFn;
            });
        });
    });

    describe('busy button - ', () => {
        let button;

        beforeEach(() => {
            button = helpers
                .fixtures({ button: '<button class="aui-button">Some Button</button>' })
                .button;
            skate.init(button);
        });

        afterEach(() => {
            $(button).remove();
        });

        it('should show spinner', () => {
            button.busy();
            expect($(SPINNER_SELECTOR).length).to.equal(1);
        });

        it('should hide spinner', () => {
            button.idle();
            expect($(SPINNER_SELECTOR).length).to.equal(0);
        });
    });

    describe('correct position - ', () => {
        let $container;

        beforeEach(() => {
            $container = $('<div style="width: 100px; height: 100px; display: inline-block; position: absolute; top: 0; left: 0"></div>');
            $testFixture.append($container);
        });

        it('should set correct spin position', (done) => {
            $container.spin();
            helpers.afterMutations(() => {
                const $svg = $(`${ SPINNER_SELECTOR }>.${ SPINNER_SELECTOR }>svg`);
                $svg.css({ 'animation': 'none', '-webkit-animation': 'none' });
                const spinnerPosition = $svg.position();
                expect(spinnerPosition.left).to.equal(40);
                expect(spinnerPosition.top).to.equal(40);
                done();
            });
        });
    });
});
