'use strict';

import $ from '../../../src/js/aui/jquery';
import banner from '../../../src/js/aui/banner';

describe('aui/banner', function () {
    it('cannot be created unless a #header is present', function () {
        try {
            expect(banner({body: 'test'})).to.throw(Error);
        } catch (e) {
            expect(e.message && e.message.indexOf('header')).to.be.above(-1);
        }
    });

    describe('API', function () {
        var header;

        beforeEach(function () {
            header = $('<div id="header"></div>');
            header.html('<p>test</p><nav class="aui-header"></nav>');
            $('#test-fixture').append(header);
        });

        it('global', function () {
            expect(AJS.banner).to.equal(banner);
        });

        it('AMD module', function (done) {
            amdRequire(['aui/banner'], function (amdModule) {
                expect(amdModule).to.equal(banner);
                done();
            });
        });

        it('should return a DOM element', function () {
            var b = banner({body: 'test banner'});
            expect(b).to.be.an.instanceof(HTMLElement);
        });

        it('are prepended to the header', function () {
            var b = banner({body: 'test banner'});
            expect(header.children().length).to.equal(3);
            expect(header.children().get(0)).to.equal(b);
        });

        it('can have a body that accepts html', function () {
            var b = banner({body: 'with an <strong>important <a href="#">hyperlink</a></strong>!'});
            expect(b.textContent, 'with an important hyperlink!');
            expect(b.querySelectorAll('strong').length).to.equal(1);
            expect(b.querySelectorAll('a').length).to.equal(1);
        });
    });
});
