require(['jquery', './toggle-server'], function($, { url, server }) {
    var ourUrl = url;

    $(document).on('change', '#toggle-controller-delay', function (e) {
        server.autoRespondAfter = e.target.value;
    });

    $(document).on('change', '#toggle-controller-success', function (e) {
        ourUrl = (e.target.checked) ? url :  'wrongUrl';
    });

    $(document).on('change', 'div.triggers aui-toggle', function (e) {
        var toggle = e.currentTarget;
        var isChecked = toggle.checked;
        toggle.busy = true;

        $.post(ourUrl, {value: isChecked})
            .done(function() {
                console.log('success');
            })
            .fail(function() {
                toggle.checked = !isChecked;
                console.error('display an error message ');
            }).always(function () {
                toggle.busy = false;
            });
    });

    $(document).on('submit', '#my-form', function(e) {
        var $result = $('#my-form-result');
        var $form = $(e.target);
        e.preventDefault();
        $result.val($form.serialize());
    });

});
