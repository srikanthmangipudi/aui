/**
 * Common JS for test pages only.
 * Do not load resources for test pages - they must be viewed in the refapp.
 */
(function () {

    window.onload = function() {
        if (window.location.protocol === 'file:') {
            var header = document.getElementsByTagName("body")[0];
            header.innerHTML = 'Error: this page must be viewed via the refapp: <a href="http://localhost:9999/ajs/">http://localhost:9999/ajs</a>';
            header.style.color = "red";
        }
    };

}());

var AUITEST = {

    // DRY, innit. (note this bit of JS predates Soy :))
    newTestDropdown: function (id,ddType) {
        var base = AJS.$('<div class="aui-dropdown2 aui-style-default"><ul><li><a href="#">Attach File</a></li><li><a href="#">Comment</a></li><li><a href="#">Edit Issue</a></li><li><a href="#">Watch Issue</a></li></ul></div>');
        var type = ddType || 'generic';

        if (type === "unstyled") {
            base.removeClass("aui-style-default");
        }
        if (type === "forcedwidth") {
            base.addClass("test-forced-width");
        }

        base.attr("id",id).appendTo("body");
        return base;
    },

    genericAlert: function () {
        alert("Dummy content clicked.");
    },

    disableTriggers: function () {
        var $triggers = AJS.$(".auitest-disabletriggers a, .auitest-disabletriggers button");
        $triggers.on("click", function() {
            AUITEST.genericAlert();
            return false;
        });
    },

    focusedPageLayoutToggles: function () {
        var $triggers = AJS.$("#aui-page-focused-small, #aui-page-focused-medium, #aui-page-focused-large, #aui-page-focused-xlarge");
        var bodyClass;
        $triggers.on("click", function() {
            bodyClass = "aui-page-focused " + AJS.$(this).attr("id");
            console.log(bodyClass);
            AJS.$("body").attr("class", bodyClass);
            return false;
        });
    }

};

AJS.$(document).ready(function(){
    AUITEST.disableTriggers();
    AUITEST.focusedPageLayoutToggles();
    AJS.$('#swap-application-button').click(function(){
        var REFAPP_PORT = '9999';
        var FLATAPP_PORT = '7000';
        var REFAPP_PREFIX = '/ajs/plugins/servlet/ajstest/test-pages/';
        var FLATAPP_PREFIX = '/pages/';
        var path = window.location.pathname;
        var host = window.location.host;
        var hostParts = host.split(':');
        var hostName = hostParts[0];
        var port = hostParts[1];

        console.log(port);

        var newPath;
        var newHREF;
        if (port === REFAPP_PORT) {
            newPath = path.replace(REFAPP_PREFIX, FLATAPP_PREFIX);
            newHREF = hostName + ':' + FLATAPP_PORT + newPath;
        } else {
            newPath = path.replace(FLATAPP_PREFIX, REFAPP_PREFIX);
            newHREF = hostName + ':' + REFAPP_PORT + newPath;
        }
        window.location.replace('http://' + newHREF);

        console.log('Going to ' + newHREF);
    });
});
