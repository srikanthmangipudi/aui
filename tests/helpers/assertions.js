export const expectBooleanAttribute = {

    isAbsent: function (el, attr) {
        expect(el.hasAttribute(attr)).to.equal(false, `isAbsent: hasAttribute ${attr}`);
        expect(el.getAttribute(attr)).to.equal(null, `isAbsent: getAttribute ${attr}`);
    },

    isPresentAndEmpty: function (el, attr) {
        expect(el.hasAttribute(attr)).to.equal(true, `isPresentAndEmpty: hasAttribute ${attr}`);
        expect(el.getAttribute(attr)).to.equal('', `isPresentAndEmpty: getAttribute ${attr}`);
    }
};

export default {
    expectBooleanAttribute
};
