'use strict';

import $ from './jquery';
import './spinner';

$.fn.spin = function spinStart() {
    return this.each(function() {
        if (!this || !this.nodeType) { return; }
        const $this = $(this);
        const data = $this.data();
        if (data) {
            const $spinnerDom = $('<aui-spinner size="small" filled></aui-spinner>');

            $this.spinStop();
            $this.append($spinnerDom);

            data.spinner = $spinnerDom;
        }
    });
};

$.fn.spinStop = function spinStop() {
    return this.each(function() {
        if (!this || !this.nodeType) { return; }
        const $this = $(this);
        const data = $this.data();
        if (data && data.spinner) {
            data.spinner.remove();
        }
    });
};
