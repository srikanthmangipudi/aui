'use strict';

import $ from './jquery';
import { fn as deprecateFn } from './internal/deprecation';
import globalize from './internal/globalize';

/**
 * Shortcut function to toggle class name of an element.
 *
 * @param {String | Element} element The element or an ID to toggle class name on.
 * @param {String} className The class name to remove or add.
 *
 * @returns {undefined}
 */
function toggleClassName (element, className) {
    if (!(element = $(element))) {
        return;
    }

    element.toggleClass(className);
}

var toggleClassName = deprecateFn(toggleClassName, 'toggleClassName', {
    sinceVersion: '5.8.0'
});

globalize('toggleClassName', toggleClassName);

export default toggleClassName;
