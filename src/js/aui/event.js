'use strict';

import $ from './jquery';
import * as logger from './internal/log';
import globalize from './internal/globalize';

/**
 * Binds events to the window object. See jQuery bind documentation for more
 * details. Exceptions are caught and logged.
 */
function bind (eventType, eventData, handler) {
    try {
        if (typeof handler === 'function') {
            return $(window).bind(eventType, eventData, handler);
        } else {
            return $(window).bind(eventType, eventData);
        }
    } catch (e) {
        logger.log('error while binding: ' + e.message);
    }
}

/**
 * Unbinds event handlers from the window object. See jQuery unbind
 * documentation for more details. Exceptions are caught and logged.
 */
function unbind (eventType, handler) {
    try {
        return $(window).unbind(eventType, handler);
    } catch (e) {
        logger.log('error while unbinding: ' + e.message);
    }
}

/**
 * Triggers events on the window object. See jQuery trigger documentation for
 * more details. Exceptions are caught and logged.
 */
function trigger (eventType, extraParameters) {
    try {
        return $(window).trigger(eventType, extraParameters);
    } catch (e) {
        logger.log('error while triggering: ' + e.message);
    }
}

globalize('bind', bind);
globalize('trigger', trigger);
globalize('unbind', unbind);

export {
    bind,
    unbind,
    trigger
};
